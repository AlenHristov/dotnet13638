﻿using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Mvc;

namespace MvcMusic.Controllers
{
    public class HelloWorldController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Welcome()
        {
            ViewBag.Message = "Hello " + User.Identity.GetUserName();

            return View();
        }
    }
}