﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMusic.Models;
using MvcMusic.Models.ViewModels;
using MvcMusic.Repositories;

namespace MvcMusic.Controllers
{
    public class SongsController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();


        // GET: Songs
        public ActionResult Index(string Genre, string Song, string Singer)
        {
            var GenreLst = new List<string>();

            var GenreQry = from d in unitOfWork.GenreRepository.Get()
                          orderby d.Name
                         select d.Name;

            GenreLst.AddRange(GenreQry.Distinct());
            ViewBag.Genre = new SelectList(GenreLst);

            var songs = from m in unitOfWork.SongRepository.Get()
                         select m;

            if (!String.IsNullOrEmpty(Song))
            {
                songs = songs.Where(s => s.Title.IndexOf(Song, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            if (!String.IsNullOrEmpty(Singer))
            {
                songs = songs.Where(z => z.Singer.IndexOf(Singer, StringComparison.OrdinalIgnoreCase) >=0);
            }

            if (!string.IsNullOrEmpty(Genre))
            {
                songs = songs.Where(x => x.Genre.Name == Genre);
            }
            var model = new List<SongVM>();
            foreach(var s in songs)
            {
                var genre = unitOfWork.GenreRepository.GetByID(s.GenreID);
                var songToAdd = new SongVM
                {
                    ID = s.ID,
                    GenreName = genre.Name,
                    ReleaseDate = s.ReleaseDate,
                    Singer = s.Singer,
                    Title = s.Title
                };
                model.Add(songToAdd);
            }
            return View(model);
        }

        // GET: Songs/Details/5
        public ViewResult Details(int id)
        {
            Song song = unitOfWork.SongRepository.GetByID(id);
            var genre = unitOfWork.GenreRepository.GetByID(song.GenreID);
            var model = new SongDetailsVM
            {
                ID = song.ID,
                ReleaseDate = song.ReleaseDate,
                Singer = song.Singer,
                Title = song.Title,
                GenreName = genre.Name
            };
            return View(model);
        }

            // GET: Songs/Create
        public ActionResult Create()
        {
            var model = new SongCreateVM();
            var genres = unitOfWork.GenreRepository.Get();
            foreach(var g in genres)
            {
                var genre = new SelectListItem
                {
                    Text = g.Name,
                    Value = g.ID.ToString()
                };
                model.Genres.Add(genre);
            }
            return View(model);
        }

        // POST: Songs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title,Singer,ReleaseDate,GenreID,Genres")] SongCreateVM model)
        {
            if (ModelState.IsValid)
            {
                var genre = unitOfWork.GenreRepository.GetByID(model.GenreID);
                if (genre == null) return View(model);
                var song = new Song
                {
                    Title = model.Title,
                    ReleaseDate = model.ReleaseDate,
                    Singer = model.Singer,
                    Genre = genre
                };
                unitOfWork.SongRepository.Insert(song);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Songs/Edit/5
        public ActionResult Edit(int id)
        {
            Song song = unitOfWork.SongRepository.GetByID(id);
            if (song == null) return RedirectToAction("Index");
            var model = new SongEditVM
            {
                ID = song.ID,
                Title = song.Title,
                Singer = song.Singer,
                ReleaseDate = song.ReleaseDate
            };
            var genres = unitOfWork.GenreRepository.Get();
            foreach (var g in genres)
            {
                var genre = new SelectListItem
                {
                    Text = g.Name,
                    Value = g.ID.ToString(),
                    Selected = g.ID == song.GenreID
                };
                model.Genres.Add(genre);
            }
            return View(model);
        }

        // POST: Songs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Singer,ReleaseDate,GenreID,Genres")] SongEditVM model)
        {
            if (ModelState.IsValid)
            {
                var genre = unitOfWork.GenreRepository.GetByID(model.GenreID);
                if (genre == null) return View(model);
                var song = new Song
                {
                    ID = model.ID,
                    Title = model.Title,
                    ReleaseDate = model.ReleaseDate,
                    Singer = model.Singer,
                    Genre = genre,
                    GenreID = genre.ID
                };
                unitOfWork.SongRepository.Update(song);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Songs/Delete/5
        public ActionResult Delete(int id)
        {
            Song song = unitOfWork.SongRepository.GetByID(id);
            var genre = unitOfWork.GenreRepository.GetByID(song.GenreID);
            var model = new SongDetailsVM
            {
                ID = song.ID,
                ReleaseDate = song.ReleaseDate,
                Singer = song.Singer,
                Title = song.Title,
                GenreName = genre.Name
            };
            return View(model);
        }

        // POST: Songs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Song song = unitOfWork.SongRepository.GetByID(id);
            unitOfWork.SongRepository.Delete(id);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
