﻿using System;
using MvcMusic.Models;

namespace MvcMusic.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        private GenericRepository<Song> songRepository;
        private GenericRepository<Genre> genreRepository;

        public GenericRepository<Song> SongRepository
        {
            get
            {

                if (this.songRepository == null)
                {
                    this.songRepository = new GenericRepository<Song>(context);
                }
                return songRepository;
            }
        }
        public GenericRepository<Genre> GenreRepository
        {
            get
            {

                if (this.genreRepository == null)
                {
                    this.genreRepository = new GenericRepository<Genre>(context);
                }
                return genreRepository;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}