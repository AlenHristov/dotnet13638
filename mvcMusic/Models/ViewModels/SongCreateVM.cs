﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusic.Models.ViewModels
{
    public class SongCreateVM
    {
        public SongCreateVM()
        {
            Genres = new List<SelectListItem>();
        }
        public string Title { get; set; }
        public string Singer { get; set; }

        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }
        public int GenreID { get; set; }
        public List<SelectListItem> Genres { get; set; }
    }
}
