﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMusic.Models.ViewModels
{
    public class GenreVM
    {
        public string Name { get; set; }
    }
}