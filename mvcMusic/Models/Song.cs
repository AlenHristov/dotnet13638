﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MvcMusic.Models
{
    public class Song
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Singer { get; set; }

        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }
        public int GenreID { get; set; }
        public Genre Genre { get; set; }
    }
    public class MusicDBContext : DbContext
    {
        public DbSet<Song> Songs { get; set; }
        public DbSet<Genre> Genres { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Song>()
                .HasRequired<Genre>(s => s.Genre)
                .WithMany(g => g.Songs)
                .HasForeignKey<int>(s => s.GenreID);
        }
    }
}
