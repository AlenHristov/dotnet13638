﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMusic.Models
{
    public class Genre
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Song> Songs { get; set; }
    }
}